package ru.malakhov.tm.listener.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.component.IConsoleProvider;
import ru.malakhov.tm.endpoint.*;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;
import ru.malakhov.tm.service.PropertyService;

@Component
public class UserRemoveByLoginListener extends AbstractListener {

    @NotNull
    @Autowired
    private IConsoleProvider consoleProvider;

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "user-remove";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    @EventListener(condition = "@userRemoveByLoginListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws AbstractException_Exception {
        System.out.println("[USER-REMOVE]");
        System.out.print("ENTER USER LOGIN:");
        @NotNull final String login = consoleProvider.nextLine();
        @Nullable final SessionDto session = propertyService.getSession();
        @Nullable final UserDto currentUser = sessionEndpoint.getUser(session);
        if (currentUser == null) {
            System.out.println("[Error]");
            return;
        }
        if (login.equals(currentUser.getLogin())) {
            String answer = "";
            do {
                System.out.print("THIS IS YOUR ACCOUNT. DELETE IT(Y/N): ");
                answer = consoleProvider.nextLine().toLowerCase();
            } while (!answer.equals("y") && !answer.equals("n"));
            if (answer.equals("n")) {
                System.out.println("[CANCEL]");
                return;
            }
            propertyService.setSession(null);
        }
        @NotNull final Result result = adminUserEndpoint.removeUserByLogin(
                session,
                login
        );
        if (result.isSuccess()) System.out.println("[OK]");
        else {
            System.out.println("MESSAGE" + result.getMessage());
            System.out.println("[FAIL]");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

}