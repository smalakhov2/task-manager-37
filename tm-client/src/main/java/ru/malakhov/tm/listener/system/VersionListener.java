package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.event.RunEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class VersionListener extends AbstractListener {

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    private void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.3.8");
    }

    @Override
    @EventListener(condition = "@versionListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        execute();
    }

    @EventListener(condition = "@versionListener.arg() == #event.name")
    public void handler(@NotNull final RunEvent event) {
        execute();
    }

    @Override
    public boolean secure() {
        return false;
    }

}
