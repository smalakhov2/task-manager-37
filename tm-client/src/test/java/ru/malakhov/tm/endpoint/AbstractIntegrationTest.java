package ru.malakhov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class AbstractIntegrationTest {

    @NotNull
    protected static final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    protected static SessionDto adminSession;

    @Nullable
    protected static SessionDto userSession;

    @BeforeClass
    public static void beforeClass() {
        adminSession = openAdminSession();
        userSession = openUserSession();
    }

    @AfterClass
    public static void afterClass() {
        sessionEndpoint.clearAllSession(adminSession);
    }

    @Nullable
    private static SessionDto openAdminSession() {
        try {
            return sessionEndpoint.openSession("admin", "admin");
        } catch (AbstractException_Exception e) {
            return null;
        }
    }

    @Nullable
    private static SessionDto openUserSession() {
        try {
            return sessionEndpoint.openSession("test", "test");
        } catch (AbstractException_Exception e) {
            return null;
        }
    }

}