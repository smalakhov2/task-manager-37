package ru.malakhov.tm.exception.unknown;

import ru.malakhov.tm.exception.AbstractException;

public final class UnknownTaskException extends AbstractException {

    public UnknownTaskException() {
        super("Error! Unknown task ...");
    }

}
