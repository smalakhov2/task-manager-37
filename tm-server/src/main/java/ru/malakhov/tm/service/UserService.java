package ru.malakhov.tm.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.*;
import ru.malakhov.tm.exception.unknown.UnknownUserException;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.repository.dto.UserDtoRepository;
import ru.malakhov.tm.repository.entity.UserRepository;
import ru.malakhov.tm.util.HashUtil;

import java.util.List;

@Setter
@Service
public final class UserService extends AbstractService<UserDto, UserDtoRepository> implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    public UserService(@NotNull final UserDtoRepository userDtoRepository) {
        super(userDtoRepository);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDto user = new UserDto(login, password, "");
        save(user);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final UserDto user = new UserDto(login, password, email);
        save(user);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final UserDto user = new UserDto(login, password, "");
        user.setRole(role);
        save(user);
    }

    @NotNull
    @Override
    public List<User> findAllEntity() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findOneEntityById(
            @Nullable final String id
    ) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public UserDto findOneDtoByLogin(
            @Nullable final String login
    ) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return dtoRepository.findByLogin(login).orElse(null);
    }

    @Nullable
    @Override
    public User findOneEntityByLogin(
            @Nullable final String login
    ) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login).orElse(null);
    }

    @Override
    @Transactional
    public void removeAll() {
        userRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOneByLogin(@Nullable final String login) throws EmptyLoginException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @Override
    @Transactional
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String password,
            @Nullable final String newPassword
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyNewPasswordException();

        @Nullable final UserDto user = findOneDtoById(userId);
        if (user == null) throw new AccessDeniedException();
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (!user.getPasswordHash().equals(passwordHash)) throw new AccessDeniedException();
        @Nullable final String newPasswordHash = HashUtil.salt(newPassword);
        if (newPasswordHash == null) throw new AccessDeniedException();
        user.setPasswordHash(newPasswordHash);
        save(user);
    }

    @Override
    @Transactional
    public void updateUserInfo(
            @Nullable final String userId,
            @Nullable final String email,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();

        @Nullable final UserDto user = findOneDtoById(userId);
        if (user == null) throw new AccessDeniedException();
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        save(user);
    }

    @Override
    @Transactional
    public void lockUserByLogin(
            @Nullable final String login
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDto user = findOneDtoByLogin(login);
        if (user == null) throw new UnknownUserException();
        user.setLocked(true);
        save(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(
            @Nullable final String login
    ) throws AbstractException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        @Nullable final UserDto user = findOneDtoByLogin(login);
        if (user == null) throw new UnknownUserException();
        user.setLocked(false);
        save(user);
    }

}