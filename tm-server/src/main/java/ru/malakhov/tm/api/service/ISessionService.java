package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.SessionDto;
import ru.malakhov.tm.dto.UserDto;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.enumerated.Role;
import ru.malakhov.tm.exception.AbstractException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.repository.dto.SessionDtoRepository;

import java.util.List;

public interface ISessionService extends IService<SessionDto, SessionDtoRepository> {

    @NotNull
    List<SessionDto> findAllDto();

    @NotNull
    List<Session> findAllEntity();

    @NotNull
    List<SessionDto> findAllDtoByUserId(
            @Nullable String userId
    ) throws EmptyUserIdException;

    @NotNull
    List<Session> findAllEntityByUserId(
            @Nullable String userId
    ) throws EmptyUserIdException;

    @Nullable
    SessionDto findOneDtoById(
            @Nullable String id
    ) throws EmptyIdException;

    @Nullable
    Session findOneEntityById(
            @Nullable String id
    ) throws EmptyIdException;

    void removeAll();

    void removeAllByUserId(
            @Nullable String userId
    ) throws EmptyUserIdException;

    void removeOneById(
            @Nullable String id
    ) throws EmptyIdException;

    @Nullable
    UserDto checkDataAccess(
            @Nullable String login,
            @Nullable String password
    ) throws AbstractException;

    @Nullable
    SessionDto open(
            @Nullable String login,
            @Nullable String password
    ) throws AbstractException;

    @Nullable
    SessionDto setSignature(@Nullable SessionDto session);

    @Nullable
    UserDto getUser(@Nullable SessionDto session) throws AbstractException;

    @NotNull
    String getUserId(@Nullable SessionDto session) throws AccessDeniedException;

    @NotNull
    List<SessionDto> getListSession(@Nullable SessionDto session) throws AbstractException;

    void close(@Nullable SessionDto session) throws AbstractException;

    void closeAll(@Nullable SessionDto session) throws AccessDeniedException;

    void validate(@Nullable SessionDto session) throws AccessDeniedException;

    void validate(@Nullable SessionDto session, @Nullable Role role) throws AbstractException;

    boolean isValid(@Nullable SessionDto session);

    void signOutByLogin(@Nullable String login) throws AbstractException;

}