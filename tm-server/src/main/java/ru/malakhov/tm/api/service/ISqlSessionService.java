package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface ISqlSessionService {

    void initFactory();

    void closeFactory();

    @NotNull
    EntityManager getEntityManager();

}