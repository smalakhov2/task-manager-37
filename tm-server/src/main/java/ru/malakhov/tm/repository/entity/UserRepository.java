package ru.malakhov.tm.repository.entity;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.entity.User;

import java.util.Optional;

@Repository
public interface UserRepository extends AbstractEntityRepository<User> {

    Optional<User> findByLogin(String login);

    @Transactional
    int deleteByLogin(String login);

}