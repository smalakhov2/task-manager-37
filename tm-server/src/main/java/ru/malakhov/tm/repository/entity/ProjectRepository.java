package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.entity.Project;

import java.util.Optional;

@Repository
public interface ProjectRepository extends AbstractEntityRepository<Project> {

    @NotNull
    @Query("SELECT e FROM Project e WHERE e.user.id=:userId")
    Iterable<Project> findByUserId(@Param("userId") String userId);

    @NotNull
    @Query("SELECT e FROM Project e WHERE e.user.id=:userId ORDER BY e.name ASC")
    Iterable<Project> findByUserIdOrderByName(@Param("userId") String userId);

    @NotNull
    @Query("SELECT e FROM Project e WHERE e.id=:id AND e.user.id=:userId")
    Optional<Project> findByIdAndUserId(
            @Param("id") String id,
            @Param("userId") String userId
    );

    @NotNull
    @Query("SELECT e FROM Project e WHERE e.user.id=:userId AND e.name=:name")
    Optional<Project> findByUserIdAndName(
            @Param("userId") String userId,
            @Param("name") String name
    );

    @Modifying
    @Transactional
    @Query("DELETE Project e WHERE e.user.id=:userId")
    int deleteByUserId(@Param("userId") String userId);

    @Modifying
    @Transactional
    @Query("DELETE Project e WHERE e.id=:id AND e.user.id=:userId")
    int deleteByIdAndUserId(
            @Param("id") String id,
            @Param("userId") String userId
    );

    @Modifying
    @Transactional
    @Query("DELETE Project e WHERE e.user.id=:userId AND e.name=:name")
    int deleteByUserIdAndName(
            @Param("userId") String userId,
            @Param("name") String name
    );

}