package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.SessionDto;

@Repository
public interface SessionDtoRepository extends AbstractDtoRepository<SessionDto> {

    @NotNull
    Iterable<SessionDto> findByUserId(String userId);

}