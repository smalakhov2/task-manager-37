package ru.malakhov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_project")
public final class Project extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    @Enumerated(value = EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "date_start")
    private Date dateStart;

    @Nullable
    @Column(name = "date_finish")
    private Date dateFinish;

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public Project(@NotNull final String name) {
        this.name = name;
    }

    public Project(
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o instanceof Project && super.equals(o)) {
            @NotNull final Project project = (Project) o;
            return Objects.equals(name, project.name)
                    && Objects.equals(description, project.description);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }

}