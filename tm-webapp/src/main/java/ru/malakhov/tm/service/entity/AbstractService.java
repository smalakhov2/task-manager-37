package ru.malakhov.tm.service.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.entity.IService;
import ru.malakhov.tm.entity.AbstractEntity;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.repository.entity.AbstractEntityRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class AbstractService<E extends AbstractEntity, R extends AbstractEntityRepository<E>> implements IService<E, R> {

    protected R repository;

    protected AbstractService(@NotNull final R repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public void save(@Nullable final E entity) {
        if (entity == null) return;
        repository.save(entity);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final Collection<E> entities) {
        if (entities == null || entities.isEmpty()) return;
        entities.removeIf(Objects::isNull);
        repository.saveAll(entities);
    }

    @Override
    @Transactional
    public void saveAll(@Nullable final E... entities) {
        if (entities == null || entities.length == 0) return;
        @NotNull final List<E> collection =
                Arrays.stream(entities).filter(Objects::nonNull).collect(Collectors.toList());
        repository.saveAll(collection);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public E findOneById(@Nullable final String id) {
        if (id == null) return null;
        return repository.findById(id).orElse(null);
    }

    @Override
    public boolean existById(@Nullable final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public void deleteById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        repository.deleteById(id);
    }

    @Override
    public void delete(@Nullable final E entity) {
        if (entity == null) return;
        repository.delete(entity);
    }

    @Override
    public void deleteAll(@Nullable final Collection<E> entities) {
        if (entities == null || entities.isEmpty()) return;
        entities.removeIf(Objects::isNull);
        repository.deleteAll(entities);
    }

    @Override
    public void deleteAll(@Nullable final E... entities) {
        if (entities == null || entities.length == 0) return;
        @NotNull final List<E> collection =
                Arrays.stream(entities).filter(Objects::nonNull).collect(Collectors.toList());
        repository.deleteAll(collection);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

}