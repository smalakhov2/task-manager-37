package ru.malakhov.tm.controller.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.api.service.dto.IProjectDtoService;
import ru.malakhov.tm.api.service.dto.ITaskDtoService;
import ru.malakhov.tm.dto.ProjectDto;
import ru.malakhov.tm.dto.TaskDto;
import ru.malakhov.tm.enumerated.Status;

import java.util.List;

@Controller
@RequestMapping("/task")
public class TaskController {

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @NotNull
    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @ModelAttribute("projects")
    public List<ProjectDto> getProjects() {
        return projectDtoService.findAll();
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteProjectGet(
            @Nullable @PathVariable("id") final String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/projects");
        if (id == null || id.isEmpty()) {
            modelAndView.setStatus(HttpStatus.BAD_REQUEST);
            return modelAndView;
        }
        taskDtoService.deleteOneById(id);
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView createProjectGet() {
        taskDtoService.create();
        return new ModelAndView("redirect:/tasks");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView view(
            @Nullable @PathVariable("id") final String id
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task_edit");
        if (id == null || id.isEmpty()) {
            modelAndView.setStatus(HttpStatus.BAD_REQUEST);
            return modelAndView;
        }
        @Nullable final TaskDto task = taskDtoService.findOneById(id);
        if (task == null) {
            modelAndView.setStatus(HttpStatus.NOT_FOUND);
            return modelAndView;
        }
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @PostMapping("/edit/{id}")
    public ModelAndView update(
            @ModelAttribute("project") TaskDto taskDto,
            @NotNull BindingResult result
    ) {
        taskDtoService.save(taskDto);
        return new ModelAndView("redirect:/tasks");
    }

}
