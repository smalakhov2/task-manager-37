package ru.malakhov.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.service.dto.IProjectDtoService;
import ru.malakhov.tm.dto.ProjectDto;

@RestController
@RequestMapping("/api/v1/project")
public class ProjectRestController {

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @PostMapping
    public void createProject(@RequestBody @Nullable final ProjectDto project) {
        projectDtoService.save(project);
    }

    @PutMapping
    public void updateProjectPut(@RequestBody @Nullable final ProjectDto project) {
        projectDtoService.save(project);
    }

    @PatchMapping
    public void updateProjectPatch(@RequestBody @Nullable final ProjectDto project) {
        projectDtoService.save(project);
    }

    @Nullable
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDto findProjectById(
            @Nullable @PathVariable("id") final String id
    ) {
        if (id == null || id.isEmpty()) return null;
        return projectDtoService.findOneById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteProjectById(
            @Nullable @PathVariable("id") final String id
    ) {
        if (id == null || id.isEmpty()) return;
        projectDtoService.deleteOneById(id);
    }

    @GetMapping(value = "/exists/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean existProjectById(
            @Nullable @PathVariable("id") final String id
    ) {
        return projectDtoService.existById(id);
    }

}