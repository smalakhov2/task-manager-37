package ru.malakhov.tm.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.service.dto.ITaskDtoService;
import ru.malakhov.tm.dto.TaskDto;

import java.util.List;

@RestController
@RequestMapping("/api/v1/tasks")
public class TaskListRestController {

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @PostMapping
    public void createTasks(@RequestBody @Nullable final List<TaskDto> entities) {
        taskDtoService.saveAll(entities);
    }

    @PutMapping
    public void updateTasksPut(@RequestBody @Nullable final List<TaskDto> entities) {
        taskDtoService.saveAll(entities);
    }

    @PatchMapping
    public void updateTasksPatch(@RequestBody @Nullable final List<TaskDto> entities) {
        taskDtoService.saveAll(entities);
    }

    @DeleteMapping
    public void deleteTasks(@RequestBody @Nullable final List<TaskDto> entities) {
        taskDtoService.deleteAll(entities);
    }

    @DeleteMapping("/all")
    public void deleteAllTasks() {
        taskDtoService.deleteAll();
    }

    @NotNull
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskDto> findAllTasks() {
        return taskDtoService.findAll();
    }

    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public long countTasks() {
        return taskDtoService.count();
    }

}