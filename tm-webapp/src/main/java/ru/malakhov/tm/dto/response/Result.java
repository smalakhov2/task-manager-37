package ru.malakhov.tm.dto.response;

import org.jetbrains.annotations.NotNull;

public class Result {

    @NotNull
    public Boolean success = true;

    @NotNull
    public String message = "";

}